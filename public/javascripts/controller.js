/**
*  Module 
*
* Description
*/
// ** Setup angular app
var turnerApp = angular.module('turnerApp', ['ui.bootstrap']);

// ** Setup controllers
turnerApp.controller('mainCntrl', mainCntrl);
turnerApp.controller('titleContentCntrl', titleContentCntrl);

// ** Main Controller method
function mainCntrl($scope, $http, $uibModal, $log){
	// ** Get the title from Mongo
	$http.get('/titles').success(function(data, status) {
		$scope.titlesData = data;
		for (var i = 0; i < data.length; i++) {
			var nomCount = 0;
			var winCount = 0;
			if(data[i].Awards) {
				for (var j = 0; j < data[i].Awards.length; j++) {
					if (data[i].Awards[j].AwardWon == true) {
						winCount++;
					} else {
						nomCount++;
					}
				}
			}
			$scope.titlesData[i].nominationCount = nomCount;
			$scope.titlesData[i].winCount = winCount;
		}
	});

	// ** UI Bootstrap method for angular modals
	$scope.open = function (title, id) {

		console.log(id);
		// ** Init for modal instance
	    var titleModal = $uibModal.open({
	      animation: true,
	      templateUrl: 'titleContent.html',
	      controller: 'titleContentCntrl',
	      size: 'lg',
	      resolve: {
	        titleData: function () {
	        	return {title: title, id: id};
	        }
	      }
	    });
  	};


}

// ** Func for modal controller
function titleContentCntrl($scope, $http, $uibModalInstance, titleData) {

  	$scope.modalTitle = titleData.title.TitleName;
  	$scope.modalGenres = titleData.title.Genres;
  	$scope.modalDesc = "Loading..."
  	console.log(titleData);

	$http.get('/titleSelected?titleId='+titleData.id).success(function(data, status) {
	    $scope.title = data;
	    $scope.modalDesc = data.Storylines[0].Description;
	});

  	// ** Dismiss modal 
  	$scope.cancel = function () {
   	 	$uibModalInstance.dismiss('cancel');
  	};

}