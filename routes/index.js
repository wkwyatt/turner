var express = require('express');
var router = express.Router();
var mongourl = 'mongodb://readonly:turner@ds043348.mongolab.com:43348/dev-challenge';
var MongoClient = require("mongodb").MongoClient;

/* GET home page. */

router.get("/", function(req, res, next) {
	res.render('index');
});

// ** Get all the data
router.get("/titles", function(req, res, next) {
	MongoClient.connect(mongourl, function(err, db) {
		if (err) {
			console.log(err);
		} else {
			db.collection("Titles").find({},{TitleName:1, TitleNameSortable:1, Genres:1, Awards:1}).sort({TitleNameSortable:1}).toArray(function(err, data) {
				res.json(data);
			});
		}
	});
});

// ** Get data from selected title
router.get("/titleSelected", function(req, res, next) {
	MongoClient.connect(mongourl, function(err, db) {
		if (err) {
			console.log(err);
		} else {
			db.collection("Titles").findOne({_id: req.query.titleId}, function(err, data) {
				res.json(data);
			});
		}
	});
});

module.exports = router;
